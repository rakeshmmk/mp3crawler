import urllib
import urllib2
import cookielib
import re
from bs4 import BeautifulSoup
import time
import logging
#from sqlalchemy.ext.declarative import declarative_base
#from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
#from sqlalchemy.orm import relationship, backref
#Base = declarative_base()

from sqlalchemy.orm import sessionmaker
from model import File,FileTag,TAG_MOVIE,TAG_SONG,SITE_MALLUDEVIL,Base


#-----------HTTP header definition starts--------------------
header={'Accept':"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"}
#header['Accept-Encoding']="gzip, deflate"
header['Accept-Language']="en-US,en;q=0.5"
header['Connection']="keep-alive"
header['User-Agent']="Mozilla/5.0 (Windows NT 6.1; WOW64; rv:16.0) Gecko/20100101 Firefox/16.0"
header['Content-type']='application/x-www-form-urlencoded'
#-----------HTTP header definition ends--------------------

####Logger config start#########################################

logger	= logging.getLogger('')


formatter = logging.Formatter('%(asctime)-6s: %(name)s - %(levelname)s - %(message)s')

consoleLogger = logging.StreamHandler()
consoleLogger.setLevel(logging.INFO)
consoleLogger.setFormatter(formatter)
logger.addHandler(consoleLogger)

fileLogger = logging.FileHandler(filename='malludevil_crawler.log')
fileLogger.setLevel(logging.INFO)
fileLogger.setFormatter(formatter)
logger.addHandler(fileLogger)

logger = logging.getLogger('')
logger.setLevel(logging.INFO)

####Logger config start#########################################

####SQLalchemy ORM declarations#################################

#Base = declarative_base()

####SQLalchemy ORM declarations END#################################





###Function to download a webpage,add logic to handle timeouts and getting blocked
def download_page(url):
	logger.info("Downloading URL:%s",url)
	time.sleep(2)
	process		= 1
	BackOffTime 	= 2
	req = urllib2.Request(url,headers=header)
	while process:
		try:
			response = urllib2.urlopen(req)
		except URLError, e:
			logger.warning("URLError Waiting for %s sec before next API call ",str(BackOffTime))
			time.sleep(BackOffTime)
			BackOffTime     = BackOffTime * 2	
		except HTTPError, e:
			self.logger.error('HTTP  error: %s ', str(e))
			logger.warning("HTTPErros Waiting for %s sec before next API call ",str(BackOffTime))
			time.sleep(BackOffTime)	
			BackOffTime     = BackOffTime * 2
		else:
			process	= 0
        the_page = response.read()
        soup = BeautifulSoup(the_page)
        return soup



base_url 	= "http://malludevil.in/"
crawl		= 1     # controls the main oop
page		= 1	# starting page number


# Create an engine that stores data in the local directory's
# sqlalchemy_example.db file.
#engine = create_engine('sqlite:///database.db')
#engine = create_engine('sqlite:///database.db')
engine = create_engine('mysql://root:greatuser@localhost/crawler') 
# Create all tables in the engine. This is equivalent to "Create Table"
# statements in raw SQL.
Base.metadata.create_all(engine)

# Bind the engine to the metadata of the Base class so that the
# declaratives can be accessed through a DBSession instance
Base.metadata.bind = engine
 
DBSession = sessionmaker(bind=engine)
# A DBSession() instance establishes all conversations with the database
# and represents a "staging zone" for all the objects loaded into the
# database session object. Any change made against the objects in the
# session won't be persisted into the database until you call
# session.commit(). If you're not happy about the changes, you can
# revert all of them back to the last commit by calling
# session.rollback()
session = DBSession()

#site_msmusiq	= Site(name="msmusiq",url="http://malludevil.in/")
#session.add(site_msmusiq)
#session.commit()
while crawl:

	url 		= "http://malludevil.in/site_442.xhtml?get-s=" +str(page)
	page		= page + 1
	soup 		= download_page(url)
	movies 		= soup.find_all("div",class_="o")
	#validate movies page
	movs_found	= 0  #flag to track if any valid movie links are found or pagination is over

	for mov in movies:
		tags	= []
		mov_url	= mov.find('a')
		if mov_url.text:
			#print mov['href'] + "\t" + mov.text
			logger.info("Parsing movie:%s from URL:%s",mov_url.text,mov_url['href'])
			movs_found	= 1
			#Add some logic to check if a movie is already processed, else process to get list fo songs
			url		= base_url +  mov_url['href']
		 	##Add to database###################
			#new_mov		= Movie(name=mov.text,url=url,site=site_msmusiq)
			#session.add(new_mov)
			#session.commit()
		
	
			#####################################
			soup            = download_page(url)
			songs         	= soup.find_all("div",class_="o")
			##validate song page
			
			for song in songs:
				song_url	= song.find('a')
				if song_url['href']:
					#print song.text
					logger.info("Parsing song:%s",song_url.text)
					##Add logic to check if song is already processed
					url 	= base_url +  song_url['href']

					##Add to database###################
					#new_song = Song(name=song.text,url=url,site=site_msmusiq,movie=new_mov)
	        	                #session.add(new_song)
        	        	        #session.commit()
					#####################################

					soup	= download_page(url)
					fle	= soup.find('a',text="[Download Here]")
					if fle['href']:
						file_url = base_url + fle['href'] 
						#print file_url
						new_file = File(name=song.text,url=file_url,site=SITE_MALLUDEVIL)
						session.add(new_file)
						session.commit()
						mov_tag	 = FileTag(tag=mov.text,tag_type=TAG_MOVIE,fle=new_file)
						session.add(mov_tag)
						song_tag  = FileTag(tag=song.text,tag_type=TAG_SONG,fle=new_file)
						session.add(song_tag)
						session.commit()
	if not movs_found == 1:	##assume that pages are numbered in ascending order
 		logger.error("Crawled all the pages, exiting!! ")	
		crawl 		= 0
		
