import urllib
import urllib2
import cookielib
import re
from bs4 import BeautifulSoup
import time
import random
from urllib2 import URLError,HTTPError
import logging

from sqlalchemy import create_engine
#from sqlalchemy.orm import relationship, backref
#Base = declarative_base()
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from model import File,Base,FileTag,TAG_MOVIE,TAG_SONG,TAG_UNKNOWN
from sqlalchemy import Column, Integer, String, DateTime,TIMESTAMP
from sqlalchemy import Sequence,ForeignKey,func
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import relationship, backref


engine = create_engine('mysql://root:greatuser@localhost/crawler')
# Create all tables in the engine. This is equivalent to "Create Table"
# statements in raw SQL.
#Base.metadata.create_all(engine)

# Bind the engine to the metadata of the Base class so that the
# declaratives can be accessed through a DBSession instance
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()

########################SQLITE DB FOR stroring quries###################
sqlite_db = create_engine('sqlite:///tutorial.db')
SQLiteBase = declarative_base()
class KeyWords(SQLiteBase):
        __tablename__   = 'keywords'
        id              = Column(Integer, Sequence('file_id_seq'), primary_key=True)
       	keyword         = Column(String(200),index=True,unique=True)

class Suggestions(SQLiteBase):
	__tablename__	= 'suggestions'
	id		= Column(Integer, Sequence('file_id_seq'), primary_key=True)
	suggestion	= Column(String(200),index=True,unique=True)
	keyword_id      = Column(Integer, ForeignKey('keywords.id'))
        keyword         = relationship("KeyWords")
class Processed(SQLiteBase):
        __tablename__   = 'processed'
        id              = Column(Integer, Sequence('file_id_seq'), primary_key=True)
	suggestion      = Column(String(200),index=True,unique=True)

#SQLiteBase.metadata.create_all(sqlite_db)

SQLiteBase.metadata.bind = sqlite_db

SQLiteSession = sessionmaker(bind=sqlite_db)
sqlite_session = SQLiteSession()


#-----------HTTP header definition STARTS--------------------
userAgents	= [ 'Mozilla/5.0 (X11; U; Linux i686; es-ES; rv:1.7.12) Gecko/20050929','Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.12) Gecko/20060607 Debian/1.7.12-1.2',
			'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.12) Gecko/20060216 Debian/1.7.12-1.1ubuntu2','Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.12) Gecko/20060205 Debian/1.7.12			-1.1','Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.12) Gecko/20060202 Fedora/1.7.12-1.5.2','Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.12) Gecko/20051203','Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.12) Gecko/20051013 Debian/1.7.12-1ubuntu1','Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0','Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20100101 Firefox/21.0','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0','Mozilla/5.0 (Windows NT 6.2; WOW64; rv:15.0) Gecko/20120910144328 Firefox/15.0.2']
#-----------HTTP header definition ENDS--------------------
####Logger config start#########################################

logger  = logging.getLogger('')


formatter = logging.Formatter('%(asctime)-6s: %(name)s - %(levelname)s - %(message)s')

consoleLogger = logging.StreamHandler()
consoleLogger.setLevel(logging.INFO)
consoleLogger.setFormatter(formatter)
logger.addHandler(consoleLogger)

fileLogger = logging.FileHandler(filename='suggest_crawler.log')
fileLogger.setLevel(logging.INFO)
fileLogger.setFormatter(formatter)
logger.addHandler(fileLogger)

logger = logging.getLogger('')
logger.setLevel(logging.INFO)

####Logger config start#########################################

def getNextSuggestion(tag_id):

	suggestion  = sqlite_session.query(Suggestions.suggestion,Suggestions.id).filter(Suggestions.id == tag_id).first()

	#print tag

	return suggestion

def RandomizeHeader():
	header={'Accept':"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"}
	#header['Accept-Encoding']="gzip, deflate"
	header['Accept-Language']="en-US,en;q=0.5"
	header['Connection']="keep-alive"
	#header['User-Agent']="Mozilla/5.0 (Windows NT 6.1; WOW64; rv:16.0) Gecko/20100101 Firefox/16.0"
	header['Content-type']='application/x-www-form-urlencoded'
	header['User-Agent'] = userAgents[random.randint(0,10)]
	return header


def SearchSuggestion(search_term):

	output		= []
	regLyrics	= re.compile("lyrics",re.I)

	time.sleep(random.randint(10,60))
	search_term	= search_term.encode('ascii', 'ignore')

	fields	= {'q':search_term}
	f		= urllib.urlencode(fields)
	#url="http://google.com/complete/search?"+ f
	#url="http://192.168.56.101/search/?"+ f
	url="http://mp3tikka.com/search/?"+ f
	header		= RandomizeHeader()
	req = urllib2.Request(url,headers=header)
	process		= 1
	BackOffTime	= 2
	while process:
                try:
                        response = urllib2.urlopen(req)
                except URLError, e:
			logger.error("URLError :%s",str(e))
                        logger.warning("URLError Waiting for %s sec before next API call ",str(BackOffTime))
                        #time.sleep(BackOffTime)
                        #BackOffTime     = BackOffTime * 2
			logger.error("Ignoring the keyword %s",search_term)
			process	= 0
                except HTTPError, e:
                        self.logger.error('HTTP  error: %s ', str(e))
                        logger.warning("HTTPErros Waiting for %s sec before next API call ",str(BackOffTime))
                        time.sleep(BackOffTime)
                        BackOffTime     = BackOffTime * 2
                else:
                        process = 0
			the_page = response.read()
			soup = BeautifulSoup(the_page)
			##add code to check if search returned any results
			if soup.body.findAll(text=re.compile("No\s+Results\s+found",re.I)):
				logger.info("No results returned for the query:%s",search_term)
	return output



def ProcessSuggestion(suggestion):
	##Check if the keyword already searched, if yes do nothing else get suggestions and add to DB
	if  sqlite_session.query(Processed.id).filter(Processed.suggestion == suggestion).first():
		logger.info("Suggestion %s processed already!!",suggestion)
	else:
		logger.info("Processing Suggestion:%s ",suggestion)

		processedsugg	= Processed(suggestion=suggestion)
		sqlite_session.add(processedsugg)
		try:
			sqlite_session.commit()
             	except SQLAlchemyError, e:
	                sqlite_session.rollback()
                       # raise e
		else:
			SearchSuggestion(suggestion)
			#Search using suggestion

if __name__ == "__main__":

#	suggestions	= GetSuggestion("1271682162876872")
		
#	if suggestions:
#		print suggestions
#	else:
#		print "None found"

	rgxsuggestion	= re.compile('mp3|song|download|song',re.I)
	rgxstopword	= re.compile('lyrics|video',re.I)
	
	for tagid in range(1,59318):
		suggestion = getNextSuggestion(tagid)
		#print suggestion 
		#tag	= filetag[0]
	
		if re.search(rgxsuggestion,suggestion[0])  and not re.search(rgxstopword,suggestion[0]):
			#print suggestion[0]
			ProcessSuggestion(suggestion[0])
		#	#print "splitting...."
		#	logger.info("Found - in the keyword, splitting and trying multple combinations")
		#	for keyword in tag.split('-'):
		#		word =  CleanTag(keyword)
		#		if word:
		#			#print word
		#			ProcessKeyword(word)
                #ProcessKeyword(CleanTag(tag))
