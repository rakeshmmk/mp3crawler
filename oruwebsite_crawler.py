import urllib
import urllib2
import cookielib
import re
from bs4 import BeautifulSoup
import time
import logging
#from sqlalchemy.ext.declarative import declarative_base
#from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
#from sqlalchemy.orm import relationship, backref
#Base = declarative_base()

from sqlalchemy.orm import sessionmaker
from model import File,Base,FileTag,TAG_UNKNOWN,SITE_ORUWEBSITE
from sqlalchemy.exc import IntegrityError

#-----------HTTP header definition starts--------------------
header={'Accept':"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"}
#header['Accept-Encoding']="gzip, deflate"
header['Accept-Language']="en-US,en;q=0.5"
header['Connection']="keep-alive"
header['User-Agent']="Mozilla/5.0 (Windows NT 6.1; WOW64; rv:16.0) Gecko/20100101 Firefox/16.0"
header['Content-type']='application/x-www-form-urlencoded'
#-----------HTTP header definition ends--------------------

####Logger config start#########################################

logger	= logging.getLogger('')


formatter = logging.Formatter('%(asctime)-6s: %(name)s - %(levelname)s - %(message)s')

consoleLogger = logging.StreamHandler()
consoleLogger.setLevel(logging.INFO)
consoleLogger.setFormatter(formatter)
logger.addHandler(consoleLogger)

fileLogger = logging.FileHandler(filename='oruwebsite_crawler.log')
fileLogger.setLevel(logging.INFO)
fileLogger.setFormatter(formatter)
logger.addHandler(fileLogger)

logger = logging.getLogger('')
logger.setLevel(logging.INFO)

####Logger config start#########################################

####SQLalchemy ORM declarations#################################

#Base = declarative_base()

####SQLalchemy ORM declarations END#################################





###Function to download a webpage,add logic to handle timeouts and getting blocked
def download_page(url):
	logger.info("Downloading URL:%s",url)
	time.sleep(2)
	process		= 1
	BackOffTime 	= 2
	req = urllib2.Request(url,headers=header)
	while process:
		try:
			response = urllib2.urlopen(req)
		except urllib2.URLError, e:
			logger.warning("URLError Waiting for %s sec before next API call ",str(BackOffTime))
			time.sleep(BackOffTime)
			BackOffTime     = BackOffTime * 2	
		except urllib2.HTTPError, e:
			self.logger.error('HTTP  error: %s ', str(e))
			logger.warning("HTTPErros Waiting for %s sec before next API call ",str(BackOffTime))
			time.sleep(BackOffTime)	
			BackOffTime     = BackOffTime * 2
		else:
			process	= 0
        the_page = response.read()
        soup = BeautifulSoup(the_page)
        return soup



base_url 	= "http://db.oruwebsite.com"
crawl		= 1     # controls the main oop
page		= 1	# starting page number


# Create an engine that stores data in the local directory's
# sqlalchemy_example.db file.
#engine = create_engine('sqlite:///database.db')
#engine = create_engine('sqlite:///database.db')
engine = create_engine('mysql://root:greatuser@localhost/crawler') 
# Create all tables in the engine. This is equivalent to "Create Table"
# statements in raw SQL.
Base.metadata.create_all(engine)

# Bind the engine to the metadata of the Base class so that the
# declaratives can be accessed through a DBSession instance
Base.metadata.bind = engine
 
DBSession = sessionmaker(bind=engine)
# A DBSession() instance establishes all conversations with the database
# and represents a "staging zone" for all the objects loaded into the
# database session object. Any change made against the objects in the
# session won't be persisted into the database until you call
# session.commit(). If you're not happy about the changes, you can
# revert all of them back to the last commit by calling
# session.rollback()
session = DBSession()

#urls	= [ "http://db.oruwebsite.com/malayalam/index.php?dir=Malayalam%20Evergreen%20Songs%20-%20special%20%20Vol%201/",]
urls	= [ "http://db.oruwebsite.com/malayalam/index.php?dir=", ]

for url in urls:

	#url 		= "http://db.oruwebsite.com/malayalam/index.php?dir="
	mp3search 	= re.compile("\.mp3$") 
	page		= page + 1
	soup 		= download_page(url)
	rows 		= soup.find_all('td',class_="autoindex_td")
	for row in rows:
		if row.a:
			if re.search(mp3search,row.a['href']):	##Found mp3, add to DB
				trailing_dot	= re.compile('^\.')
				song_url = base_url +"/malayalam" + trailing_dot.sub('',row.a['href'])
				logger.info("Found mp3 %s adding url:%s to db ",row.a.text,song_url)
				##add file into DB and tag it with file name
				try:
					new_file = File(name=row.a.text,url=song_url,site=SITE_ORUWEBSITE)
					session.add(new_file)
					session.commit()
					mov_tag  = FileTag(tag=row.a.text,tag_type=TAG_UNKNOWN,fle=new_file)
					session.add(mov_tag)
					session.commit()
				except IntegrityError:
					logger.exception("Duplicate entry for url%s",song_url)
					logger.info("Rolling back session to recover from integrity Exception")
					session.rollback()
			elif re.search('\[dir\]',row.next_sibling.next_sibling.text):
				logger.info("Found a directory url:%s adding to list",base_url + row.a['href'])
				urls.append(base_url + row.a['href'])				

logger.error("Crawled all the pages, exiting!! ")	

