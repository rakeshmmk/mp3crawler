# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem
#import eyed3
import os
from scrapy.conf import settings
from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import sessionmaker
from spiders.model import File,Base,FileTag,TAG_MOVIE,TAG_SONG,SITE_WAPMALAYALAM,TAG_TITLE,TAG_ALBUM,TAG_ARTIST
from crawlers.items import FilesItem
from hurry.filesize import size
from scrapy import log
from pybloom import BloomFilter

class SqlAlchemyPipeLine(object):
	def __init__(self):
		engine = create_engine(settings['SQLALCHEMY_ENGINE_URL'])
		Base.metadata.bind = engine
		DBSession = sessionmaker(bind=engine)
		self.session = DBSession()

	def process_item(self,item,spider):
		#print "SQL "
		#print item['files']
		chk_file = self.session.query(File).filter(File.url == item['link']).first()

		if chk_file:
			log.msg("<SPIDER :%s> Trying to add an Item already in DB"%(spider.spider_domain),level=log.DEBUG)
			if settings['DOWNLOAD_MP3_FILE']:# Updated file detals only if mp3 downloading is enabled
				chk_file.file_size	= item['file_size']
				chk_file.file_bitrate	= item['file_bitrate']
				chk_file.file_time	= item['file_time']
				self.session.add(chk_file)
				self.session.commit()
		else:
			log.msg("<SPIDER :%s> Adding a fresh mp3 into DB"%(spider.spider_domain),level=log.DEBUG)	
			new_file = File(name=item['name'],url=item['link'],site=item['site'],landing_url=item['landing_url'],\
				file_size=item['file_size'],file_bitrate=item['file_bitrate'],file_time=item['file_time'])
			self.session.add(new_file)
			try:
				self.session.commit()
			except SQLAlchemyError, e:
				self.session.rollback()
				raise e
			else:
				for tag in item['tags']:
					mov_tag  = FileTag(tag=tag['value'],tag_type=tag['type'],fle=new_file)
					self.session.add(mov_tag)
				try:
					self.session.commit()
				except SQLAlchemyError, e:
					self.session.rollback()
					raise e


		#self.log("Called Pipeline!!!")	


class DuplicatesPipeline(object):

	def __init__(self):
		#self.links_seen = set()
		self.links_seen = BloomFilter(2000000, 0.00001)

	def process_item(self, item, spider):
              #  print "DUP "
              #  print item['files']

		if item['link'] in self.links_seen:
			log.msg("<SPIDER :%s> Trying to Process and already seen item,dropping now"%(spider.spider_domain),level=log.DEBUG)
			raise DropItem("Duplicate Link found: %s" % item)
		else:
			self.links_seen.add(item['link'])
		
		return item

class MusicBuzzPipeline(object):

	def process_item(self,item,spider):
	
		if item['site'] == "musicbuzz.info":
			print "-----------------------------------------------------"
			print item	
			raise DropItem("Dropping Item: %s" % item)
		return item

class MP3DetailsPipeline(object):

        def process_item(self,item,spider):
		if settings['DOWNLOAD_MP3_FILE']:
			audiofile = eyed3.load(item['path'])	
			if audiofile.info:                
				if audiofile.tag.title:
					item['tags'].append({ 'type':TAG_TITLE,'value':audiofile.tag.title})
				if audiofile.tag.artist:
					item['tags'].append({ 'type':TAG_ARTIST,'value':audiofile.tag.artist})
				if audiofile.tag.album:
					item['tags'].append({ 'type':TAG_ALBUM,'value':audiofile.tag.album})
				if audiofile.info.bit_rate_str:
					item['file_bitrate'] = audiofile.info.bit_rate_str
				if audiofile.info.time_secs:
					item['file_time'] = str(audiofile.info.time_secs/60) + ":" + str(audiofile.info.time_secs%60)
				if audiofile.info.size_bytes:
					item['file_size'] = size(audiofile.info.size_bytes)
			       

			else:
				raise DropItem("Dropping Item: %s as its not an actual mp3file" % item)

			os.remove(item['path'])	
		else:
			item['file_bitrate'] 	= ''
			item['file_time']	= ''
			item['file_size']	= ''
		return item



class DebugPipeline(object):

        def process_item(self, item, spider):
		print "--------------------------------------------------------------"
		print item
		print "##############################################################"
