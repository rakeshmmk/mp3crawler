from scrapy.http import Request
from scrapy import log,signals
from scrapy.exceptions import IgnoreRequest
import re
from scrapy.utils.httpobj import urlparse_cached
from pybloom import BloomFilter
from scrapy.xlib.pydispatch import dispatcher
DOWNLOAD_BODY	= 1

class MimeTypeHandlerMiddleware(object):
	'''
		this middleware will identify the filetype of a URL by downloading headers aloen first, and only allowed file types are fully downloaded

	'''
	def __init__(self):
		self.domains_seen = set()
		#Create a bloomfilter as suggested in http://alexeyvishnevsky.com/?p=26
		#bloom filter has issue with multiple spiders running as closing one will clear the filter
		self.request_seen = BloomFilter(2000000, 0.00001)	
		dispatcher.connect(self.spider_closed, signals.spider_closed)
		#self.request_seen = set()

	def spider_closed(self,spider):
		"""This function will be called when spider closes.

        	"""
        	#self.request_seen = None

	
	def process_request(self, request, spider):
		"""
			the scrapy documention said that:
				If it returns a Request object, the returned request will be rescheduled (in the 
				Scheduler) to be downloaded in the future. The callback of the original request
				will always be called. If the new request has a callback it will be called with the
				response downloaded, and the output of that callback will then be passed to 
				the original callback. If the new request doesn.t have a callback, the response 
				downloaded will be just passed to the original request callback.

			if the request is coming from the spider,change method to HEAD instead of GET and download only headers.
			If headers are already processed,don't do anything just return the request	
		
		"""
		if request.meta.get('header_process',False) : #header processing already done or ignored, Download body directly
			return None
		else: ##header needs to be processed change request method to HEAD instead of get
			log.msg("'<SPIDER :%s> Marking URL:%s for header download"%(spider.spider_domain,request.url) , level=log.DEBUG)
			request.meta['header_process'] = 1
			return request.replace(method = "HEAD",dont_filter=True)
			
	def process_response(self,request, response, spider):
		"""
			from scrapy doc
			If it returns a Response (it could be the same given response, or a brand-new one), that response will continue to be 
			processed with the process_response() of the next middleware in the chain.
			If it returns a Request object, the middleware chain is halted and the returned request is rescheduled 
			to be downloaded in the future. This is the same behavior as if a request is returned from process_request().
			If it raises an IgnoreRequest exception, the errback function of the request (Request.errback) is called. 
			If no code handles the raised exception, it is ignored and not logged (unlike other exceptions).			
	
			If the request method is HEAD check the Content-type and decide what to do
				-IF the content type is text/html and the url is from an allowed domain
					 mark content_type as html and change method to GET and return the request.
					this will download the html body and gave it for parser for processing
				-If content type is mp3,mark content_type as mp3 and directly return the response
			If the request method is GET directly return the response

		"""
		##Create a regex from allowed domain attribute to check the response URL
		allowed_domains = getattr(spider, 'allowed_domains', None)
		if not allowed_domains:
   	        	return re.compile('') # allow all by default
        	regex = r'^(.*\.)?(%s)$' % '|'.join(re.escape(d) for d in allowed_domains)
		#log.msg("Allowed domains for URL:%s is :%s"%(request.url,allowed_domains))	
		###END of regex build code############################

		if request.method == "GET":
			log.msg(format="<SPIDER :%(spdomain)> Downloaded response for GET request to %(requrl)r:%(response)s",level=log.DEBUG, 
					spider=spider,spdomain=spider.spider_domain,requrl=request.url, response=response)
			return response
		elif request.method == "HEAD":
			##-------------START DUPLICATE REQUEST FILTERING----------------------------------------
			if request.url in self.request_seen:
				raise IgnoreRequest
			self.request_seen.add(request.url)
			##-------------END DUPLICATE REQUEST FILTERING------------------------------------------
			#Check Content type header
			content_type = response.headers.get('Content-Type',False)
			if content_type:	
				if content_type.find('text/html') > -1 :
				#	request.meta['header_process'] = False
					##Findout if the response URL points to one of the allowed domains,else stop processing
					host = urlparse_cached(response).hostname or ''
					if re.compile(regex).search(host):		
						log.msg("<SPIDER :%s> Found html content at URL:%s changing method to GET"%(spider.spider_domain,response.url) , level=log.DEBUG)
						req = request.replace(dont_filter=True)	
						return req.replace(method ="GET")
					else:
					##HTML page of a non-allowed domain, ignore further processing	
						domain = urlparse_cached(response).hostname
						if domain and domain not in self.domains_seen:
							self.domains_seen.add(domain)
                        				log.msg(format="<SPIDER :%(spdomain)> Filtered offsite request to %(domain)r: %(response)s",
                                					level=log.DEBUG,spdomain=spider.spider_domain, spider=spider, domain=domain, response=response)
                        				#self.stats.inc_value('offsite/domains', spider=spider)
                    				#self.stats.inc_value('offsite/filtered', spider=spider)
						raise IgnoreRequest

                                elif content_type.find('audio/mpeg') > -1 :
                                #       request.meta['header_process'] = False
                                        log.msg("<SPIDER :%s> Found MP3 content at URL:%s returning the response"%(spider.spider_domain,response.url) , level=log.DEBUG)
                                        #response.meta['content_type']	= 'mp3'
                                        return response

				else:
					log.msg("<SPIDER :%s> Found non html/mp3 content at URL:%s ignoring the request"%(spider.spider_domain,response.url),level=log.DEBUG)
					raise IgnoreRequest
			else:
				log.msg("<SPIDER :%s> Didn't find any Content Type at URL:%s ignoring the request"%(spider.spider_domain,response.url), level=log.DEBUG)
				raise IgnoreRequest
