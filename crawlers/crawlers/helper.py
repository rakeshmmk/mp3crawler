import urllib
import urllib2
import cookielib
import re
from bs4 import BeautifulSoup
import time

#-----------HTTP header definition starts--------------------
header={'Accept':"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"}
#header['Accept-Encoding']="gzip, deflate"
header['Accept-Language']="en-US,en;q=0.5"
header['Connection']="keep-alive"
header['User-Agent']="Mozilla/5.0 (Windows NT 6.1; WOW64; rv:16.0) Gecko/20100101 Firefox/16.0"
#header['Content-type']='application/x-www-form-urlencoded'
header['Cookie']='PHPSESSID=b4d058dd174d85a0290b1ff047660b94; SongList=794'
#header['Referer']= ' http://www.musicbuzz.info/player/musicplayer.swf?sid=0.39865312826748855'

#-----------HTTP header definition ends--------------------

###Function to download a webpage,add logic to handle timeouts and getting blocked
def download_page(url,refferer):
        #logger.info("Downloading URL:%s",url)
        time.sleep(2)
        process         = 1
        BackOffTime     = 2
        req = urllib2.Request(url,headers=header)
        while process:
                try:
                        response = urllib2.urlopen(req)
                except URLError, e:
                        #logger.warning("URLError Waiting for %s sec before next API call ",str(BackOffTime))
                        time.sleep(BackOffTime)
                        BackOffTime     = BackOffTime * 2
                except HTTPError, e:
                        #self.logger.error('HTTP  error: %s ', str(e))
                       # logger.warning("HTTPErros Waiting for %s sec before next API call ",str(BackOffTime))
                        time.sleep(BackOffTime)
                        BackOffTime     = BackOffTime * 2
                else:
                        process = 0
        the_page = response.read()
        soup = BeautifulSoup(the_page)
        return soup





if __name__ == "__main__":
	print download_page("http://www.musicbuzz.info/load/load.xml",'')

