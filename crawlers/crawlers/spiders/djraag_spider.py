#!/usr/bin/env python
# encoding=utf-8

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.http import FormRequest
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from scrapy import log
from crawlers.items import FilesItem
import sys
import re
import os


reload(sys)
sys.setdefaultencoding('utf-8')
from model import TAG_MOVIE,TAG_SONG,SITE_WAPMALAYALAM




class DjRagSpider(CrawlSpider):
	name = "djraag"
	allowed_domains = ["djraag.net","w2.myfunia.net"]
	start_urls = [
		"http://djraag.net/"
	#	"http://w2.myfunia.net/m/page/allmusic/top20.php?&id=2"
	]
	rules = (
		Rule(SgmlLinkExtractor(allow_domains=('w2.myfunia.net',),allow=('download.\php', )), callback='parse_hansard_item'),
		Rule(SgmlLinkExtractor(allow=(),unique=True)),

		#When we get to specific doc do this

		)

	def parse_hansard_item(self, response):
		self.log('parse_hansard_item called for: %s' % response.url, level=log.INFO)
		hxs 		= HtmlXPathSelector(response)
		files		= FilesItem()
		item 		= {}
		tags		= []
		items		= []	
		#Direct links won't work in this site, hence saving page location instead of mp3 loc
		#item['link']	= hxs.xpath('/html/body/p/a[contains(text(),"Download In")]/@href').extract()[1]
		#print item['link']
		
		#item['name']	= item['link'].split("/")[-1] 
		if hxs.xpath('/html/body/p/text()').extract()[0]:
	 		tag             = { 'type':TAG_SONG}
			tag['value']	= hxs.xpath('/html/body/p/text()').extract()[0]
			tags.append(tag)
		if hxs.xpath('/html/body/p/text()').extract()[1]:
			tag		= { 'type':TAG_MOVIE}
			tag['value']	=  hxs.xpath('/html/body/p/text()').extract()[1]
			tags.append(tag)
		item['tags']	= tags
		item['landing_url']	= response.url
		item['site']	= "djraag.net"
		files['files']	= []
		#The site has both 48Kbps and 128 Kbps mp3 files. Extract both of them
		if hxs.xpath('/html/body/p/a[contains(text(),"Download In")]/@href').extract()[1]:
			item['link']   = hxs.xpath('/html/body/p/a[contains(text(),"Download In")]/@href').extract()[1]
			item['name']	= item['link'].split("/")[-1] 
			files['files'].append(dict(item))
                if hxs.xpath('/html/body/p/a[contains(text(),"Download In")]/@href').extract()[0]:
                        item['link']   = hxs.xpath('/html/body/p/a[contains(text(),"Download In")]/@href').extract()[0]
			item['name']	= item['link'].split("/")[-1] 
                        files['files'].append(dict(item))

		#files	= items
		return files
