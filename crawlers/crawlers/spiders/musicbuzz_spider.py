#!/usr/bin/env python
# encoding=utf-8

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.http import FormRequest
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from scrapy import log
from crawlers.items import FilesItem


import sys
import re
import os


reload(sys)
sys.setdefaultencoding('utf-8')
from model import TAG_MOVIE,TAG_SONG,SITE_WAPMALAYALAM,TAG_UNKNOWN




class DjRagSpider(CrawlSpider):
	name = "musicbuzz"
	allowed_domains = ["musicbuzz.info"]
	start_urls = [
		#"http://musicbuzz.info"
		"http://www.musicbuzz.info/malayalam/movie/classmates/"	
	]
	rules = (
		Rule(SgmlLinkExtractor(allow_domains=('musicbuzz.info',),allow=('songs-download', )), callback='parse_hansard_item'),
		Rule(SgmlLinkExtractor(allow=(),unique=True)),

		#When we get to specific doc do this

		)

	def parse_hansard_item(self, response):
		self.log('parse_hansard_item called for: %s' % response.url, level=log.INFO)
		hxs 		= HtmlXPathSelector(response)
		files		= FilesItem()
		item 		= {}
		tags		= []
		items		= []	
		#Direct links won't work in this site, hence saving page location instead of mp3 loc
		#item['link']	= hxs.xpath('/html/body/p/a[contains(text(),"Download In")]/@href').extract()[1]
		#print item['link']
		
		#item['name']	= item['link'].split("/")[-1] 
		files['files']  = []

		songs_div	= hxs.xpath('//div[@id="songlist"]')
	
		for tr in songs_div.xpath(".//tr"):
			item           	= {}
			if tr.xpath('.//td/input[contains(@name,"songname")]'):
				file_id 	= tr.xpath('.//td')[0].xpath('.//input/@value').extract()[0]
				file_name 	= tr.xpath('.//td')[1].xpath('.//strong/text()').extract()[0]
			#	print "--------------------------------------------------------"
			#	print file_name
			#	print file_id
			#	print "--------------------------------------------------------"
				#tags.append({'type':TAG_UNKNOWN,'value':file_name})
				item['tags']            = [{ 'type':TAG_UNKNOWN,'value':file_name},]
				item['landing_url']     = response.url
				item['site']    = "musicbuzz.info"
				item['file_id']	= file_id
				item['name']	= file_name
				files['files'].append(dict(item))
					
		#files	= items
		return files
