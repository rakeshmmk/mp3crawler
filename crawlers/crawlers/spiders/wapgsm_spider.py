#!/usr/bin/env python
# encoding=utf-8

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.http import FormRequest
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from scrapy import log
from crawlers.items import FileItem
import sys
import re
import os


reload(sys)
sys.setdefaultencoding('utf-8')
from model import TAG_MOVIE,TAG_SONG,SITE_WAPMALAYALAM




class WapGsmSpider(CrawlSpider):
	name = "wapgsm"
	allowed_domains = ["wapgsm.in"]
	start_urls = [
		"http://wapgsm.in/Songs/"
	]
	rules = (
		Rule(SgmlLinkExtractor(allow=('index\.php', ),unique=True)),

		#When we get to specific doc do this
		Rule(SgmlLinkExtractor(allow=('file.\php', )), callback='parse_hansard_item'),

		)

	def parse_hansard_item(self, response):
		self.log('parse_hansard_item called for: %s' % response.url, level=log.INFO)
		hxs 		= HtmlXPathSelector(response)
		base_url	= "http://wapgsm.in/Songs/"
		item 		= FileItem()
		tags		= []
	
		#Direct links won't work in this site, hence saving page location instead of mp3 loc
		item['link']	= response.url
		#print item['link']
		
		string 		= base_url+hxs.select("//html/body/div[8]/div/div").extract()[0]
		item['name']	= re.search(re.compile('<b>File Name:<\/b>(.+)<br><b>Size'),string).group(1)
		tag		= { 'type':TAG_MOVIE}
		tag['value']	= re.search(re.compile('<br>Album/Movie: (.+)<br>Ar'),string).group(1)



		tags.append(tag)
		item['tags']	= tags
		item['site']	= "wapgsm.in"

		return item
