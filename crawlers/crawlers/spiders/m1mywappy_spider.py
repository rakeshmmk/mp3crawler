#!/usr/bin/env python
# encoding=utf-8

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.http import FormRequest
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from scrapy import log
from crawlers.items import FileItem
import sys
import re
import os


reload(sys)
sys.setdefaultencoding('utf-8')
from model import TAG_MOVIE,TAG_SONG,SITE_WAPMALAYALAM




class DjRagSpider(CrawlSpider):
	name = "m1mywappy"
	allowed_domains = ["http://m1.mywappy.com",]
	start_urls = [
		"http://djraag.net/"
	]
	rules = (
		Rule(SgmlLinkExtractor(allow_domains=('w2.myfunia.net',),allow=('download.\php', )), callback='parse_hansard_item'),
		Rule(SgmlLinkExtractor(allow=(),unique=True)),

		#When we get to specific doc do this

		)

	def parse_hansard_item(self, response):
		self.log('parse_hansard_item called for: %s' % response.url, level=log.INFO)
		hxs 		= HtmlXPathSelector(response)

		item 		= FileItem()
		tags		= []
	
		#Direct links won't work in this site, hence saving page location instead of mp3 loc
		item['link']	= hxs.xpath('/html/body/p/a[contains(text(),"Download")]/@href').extract()[0]
		#print item['link']
		
		item['name']	= item['link'].split("/")[-1] 
		if hxs.xpath('/html/body/p/text()').extract()[0]:
	 		tag             = { 'type':TAG_SONG}
			tag['value']	= hxs.xpath('/html/body/p/text()').extract()[0]
			tags.append(tag)
		if hxs.xpath('/html/body/p/text()').extract()[1]:
			tag		= { 'type':TAG_MOVIE}
			tag['value']	=  hxs.xpath('/html/body/p/text()').extract()[1]
			tags.append(tag)
		item['tags']	= tags
		item['site']	= "djraag.net"

		return item
