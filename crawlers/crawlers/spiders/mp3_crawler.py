from urlparse import urlparse
from scrapy.http import Request, HtmlResponse
from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from crawlers.items import FileItem
from scrapy.utils.response import get_base_url
from scrapy.utils.url import urljoin_rfc
from scrapy.exceptions import CloseSpider
import urllib
from scrapy import log,signals
from model import TAG_UNKNOWN,TAG_ANCHOR_TEXT,StartUrl,Base
from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import sessionmaker
from scrapy.conf import settings
import random
import re
import os,sys
import logging
from scrapy.log import ScrapyFileLogObserver
from pybloom import BloomFilter
from scrapy.xlib.pydispatch import dispatcher
import random
class FollowAllSpider(BaseSpider):

    name = 'mp3crawler'
    
    def __init__(self, **kw):
        super(FollowAllSpider, self).__init__(**kw)
	self.urls	= []
	self.allowed_domains = []
	#self.url	=  url = kw.get('url') or sys.exit("No URL Specified, exiting the crawler")
	self.url	=  url = kw.get('url') or  'http://www.mymp3song.com/'
	#self.allowed_domains = [urlparse(self.url).hostname.lstrip('www.'),]
	self.allowed_domains = [urlparse(self.url).hostname,]
	self.spider_domain = urlparse(self.url).hostname
	self.log("<SPIDER :%s> Opening Spider for URL:%s with allowed Domains:%s "%(self.spider_domain,self.url,self.allowed_domains),level=log.INFO)	
        self.link_extractor = SgmlLinkExtractor()
        self.cookies_seen = set()
	#self.links_seen	= set()
	#Using bloom filter to reduce memory requirement
	self.links_seen	= BloomFilter(2000000, 0.00001)
	##Register a handler to be called when spider closes
	dispatcher.connect(self.spider_closed, signals.spider_closed)
	#self.log	= log
	if settings['MULTI_LOG']:
		##Adding simultaneous logging to file as well
		##Example code from http://stackoverflow.com/questions/8532252/scrapy-logging-to-file-and-stdout-simultaneously-with-spider-names
		logname	= settings['MULTI_LOG'] 
		logfile = open(logname, 'w')
		log_observer = ScrapyFileLogObserver(logfile, level=logging.DEBUG)
		log_observer.start()



    def spider_closed(self, spider):
	"""This function will be called when spider closes.Add all cleaning up items in this

	"""
#	self.links_seen = None
	self.log("<SPIDER :%s> Closing Spider for URL:%s with allowed Domains:%s "%(spider.spider_domain,spider.url,spider.allowed_domains),level=log.INFO)

    def start_requests(self):
	"""Create request from start URL


	"""
	#r =[]
	#for url in self.urls:
		#r.append(Request(url, callback=self.parse))
	#return r
        return [Request(self.url, callback=self.parse)]

    def parse(self, response):
        """Parse a PageItem and all requests to follow

		this function will identify the content_type flag set on a response and accordingly
		If the type is html:
			find all the links on a given page and filter out all the links which are already processed using url signature
			For each link corresponding anchor text also is extracted and saved as meta to create a new request
		If the content type is mp3:
			Create an item with the provided mp3file name,anchor text,url and landing url and return the item

	@url http://www.scrapinghub.com/
	@returns items 1 1
	@returns requests 1
	@scrapes url title foo
	"""
	r = []
	content_type = response.headers.get('Content-Type',False)
	self.log("<SPIDER :%s> Parsing URL:%s CONTENT TYPE:%s "%(self.spider_domain,response.url,content_type), level=log.DEBUG)
	if content_type:	
		if content_type.find('audio/mpeg') > -1 :	
			#Link to an mp3 file,construst an item and return
			#item		= response.meta.get('item',False)
			item                    = FileItem()
			anchor_text 		= response.meta.get('anchor_text',' ')
			landing_page		= response.headers.get('Referer',' ')
			item['site']		= response.meta.get('site',' ')
			item['tags']            = [{'type':TAG_ANCHOR_TEXT,'value':anchor_text},]
			item['landing_url'] 	= response.meta.get('landing_url',' ')
			self.log('<SPIDER :%s> Found mp3 file at %s' % (self.spider_domain,response.url), level=log.DEBUG)
			for part in response.url.split('/'):
				part    = re.sub('[^a-zA-Z0-9\.]', ' ', part)
				if re.search(re.compile("\.mp3",re.I),part):
					item['tags'].append({ 'type':TAG_UNKNOWN,'value':part})
					item['name']        = part
			item['link']	= response.url
			return item
					
			
		elif content_type.find('text/html') > -1 :	
			#extract all links
		#	self.log("Response:%s"%( response))
			links_sel   = HtmlXPathSelector(response).xpath("//a")
			base_url = get_base_url(response)	
			for link_sel in links_sel:
		#		if link_sel not in self.links_seen:
				if link_sel.xpath('.//@href'):
					link                    = link_sel.xpath('.//@href').extract()[0]
					if link not in self.links_seen:
						self.links_seen.add(link)
						url			= urljoin_rfc(base_url, link)
						url	           	= urllib.unquote_plus(url)	
						site            	= urlparse(response.url).hostname.lstrip('www.')
						#Create a request with the found link,before start processing a page check if the response URL is part of allowed domain
						req 			= Request(url,callback=self.parse, headers={'Referer':response.url},dont_filter=False)
                                                if link_sel.xpath('.//text()'):
                                                        #anchor_text             = link_sel.xpath('.//text()').extract()[0]
							req.meta['anchor_text']	= link_sel.xpath('.//text()').extract()[0]
						req.meta['site']	= site
						req.meta['landing_url']	= response.url
						r.append(req)
	  
			#r.extend(self._extract_requests(response))
			return r
		else:
			self.log("<SPIDER :%s> Response with Unknown Content type:%s from URL:%s"%(self.spider_domain,content_type,response.url),level=log.ERROR)
			return None
	else:
        	self.log("<SPIDER :%s> Response with No Content type from URL:%s"%(self.spider_domain,response.url),level=log.ERROR)
                return None
