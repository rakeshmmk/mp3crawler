#!/usr/bin/env python
# encoding=utf-8

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.http import FormRequest
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from scrapy import log
from crawlers.items import FilesItem
import sys
import re
import os
import urllib

reload(sys)
sys.setdefaultencoding('utf-8')
from model import TAG_MOVIE,TAG_SONG,SITE_WAPMALAYALAM,TAG_UNKNOWN




class DjRagSpider(CrawlSpider):
	name = "malludevil"
	allowed_domains = ["malludevil.in"]
	start_urls = [
		#"http://malludevil.in/site_493.xhtml"
		"http://malludevil.in/site_bhaiyya-my-brother-mp3-songs-download.xhtml",
	]
	rules = (
		Rule(SgmlLinkExtractor(allow_domains=('malludevil.in',),allow=('xhtml\?get-file=\d+', )), callback='parse_hansard_item'),
		Rule(SgmlLinkExtractor(allow=('malludevil.in'),unique=True)),

		#When we get to specific doc do this

		)

	def parse_hansard_item(self, response):
		self.log('parse_hansard_item called for: %s' % response.url, level=log.INFO)
		hxs 		= HtmlXPathSelector(response)
		files		= FilesItem()
		link		= hxs.select("//a[contains(text(),'Download Here')]").select('@href').extract()[0]
		tags		= []	
		item		= {}
		url		= "http://malludevil.in" + link
		link		= urllib.unquote_plus(link)
		match		= re.search(re.compile('\/download.+\/(.+)\.mp3'),link)
		if match:
			files['files']		= []
			fileName	= match.group(1)
			#fileName	= re.sub('[^a-zA-Z0-9\+]', ' ', fileName)
			#print "-------------------------------FILE----------------------------" + fileName		
			item['tags']    	= [{ 'type':TAG_UNKNOWN,'value':fileName},]
   	             	item['landing_url']     = response.url
                	item['site']    	= "malludevil.in"
			item['name']		= fileName
			item['link']		= url
			files['files'].append(dict(item))
		#print link

		return files
