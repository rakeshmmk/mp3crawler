#!/usr/bin/env python
# encoding=utf-8

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.http import FormRequest
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from scrapy import log
from crawlers.items import FilesItem
import sys
import re
import os
import urllib

reload(sys)
sys.setdefaultencoding('utf-8')
from model import TAG_MOVIE,TAG_SONG,SITE_WAPMALAYALAM,TAG_UNKNOWN




class DjRagSpider(CrawlSpider):
	name = "musicnymph"
	allowed_domains = ["musicnymph.com"]
	start_urls = [
		#"http://malludevil.in/site_493.xhtml"
	#	"http://musicnymph.com",
	#	"http://www.musicnymph.com/music_track/Tamil/films/1/7am%20Arivu/",
		"http://www.musicnymph.com",
	]
	rules = (
		Rule(SgmlLinkExtractor(allow_domains=("musicnymph.com",),allow=('\.mp3','\.MP3','\.Mp3','\.mP3' )), callback='parse_hansard_item'),
		Rule(SgmlLinkExtractor(allow=('musicnymph.com'),unique=True)),

		#When we get to specific doc do this

		)

	def parse_hansard_item(self, response):
		self.log('parse_hansard_item called for: %s' % response.url, level=log.INFO)
		hxs 		= HtmlXPathSelector(response)
		files		= FilesItem()
		#link		= hxs.select("//a[contains(text(),'Download Here')]").select('@href').extract()[0]
		tags		= []	
		item		= {}
		url		= "http://musicnymph.com" + link
		link		= urllib.unquote_plus(response.url)
		fileName	= link.split('/')[-1]
		#match		= re.search(re.compile('\/download.+\/(.+)\.mp3'),link)
		
		files['files']		= []
		#print "-------------------------------FILE----------------------------" + fileName		
		item['tags']    	= [{ 'type':TAG_UNKNOWN,'value':fileName},]
		item['landing_url']     = response.url
		item['site']    	= "musicnymph.com"
		item['name']		= fileName
		item['link']		= response.url
		files['files'].append(dict(item))
		#print link

		return files
