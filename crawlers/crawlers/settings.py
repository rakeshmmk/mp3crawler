# Scrapy settings for crawlers project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'crawlers'

SPIDER_MODULES = ['crawlers.spiders']
NEWSPIDER_MODULE = 'crawlers.spiders'

###Define Pipe lines to be activated
ITEM_PIPELINES = {
    'crawlers.pipelines.MusicBuzzPipeline': 100,
    'crawlers.pipelines.DuplicatesPipeline': 300,
    'crawlers.pipelines.MP3DetailsPipeline': 700,
    'crawlers.pipelines.SqlAlchemyPipeLine': 800,
    #'crawlers.pipelines.DebugPipeline': 900
 
}
SPIDER_MIDDLEWARES = {
    'scrapy.contrib.spidermiddleware.offsite.OffsiteMiddleware': None,
}
DOWNLOADER_MIDDLEWARES = {
    'crawlers.middlewares.MimeTypeHandlerMiddleware': 50,
}

#Disable duplicate filtering as it handled in the custom midleware
DUPEFILTER_CLASS = "scrapy.dupefilter.BaseDupeFilter"

DOWNLOAD_DELAY = 2    # 250 ms of delay

SQLALCHEMY_ENGINE_URL = 'mysql://root:greatuser@localhost/crawler?charset=utf8'

##--------------LOG SETTINGS START----------------------------------------
LOG_LEVEL = 'DEBUG'
LOG_FILE = 'mp3crawler.log'
##To have both console logging and file logging, useful while running in console;
##comment the above log file and enable the below one
#MULTI_LOG = 'mp3crawler.log'

##--------------LOG SETTINGS END----------------------------------------


##############Broad Crawl Settings Start####################################

REDIRECT_ENABLED=True
CONCURRENT_REQUESTS = 100
#LOG_LEVEL = 'INFO'
##############Broad Crawl Settings END####################################

FILE_DIR	= '/home/user/mp3crawler/files/'
DOWNLOAD_MP3_FILE = 0

#changing to bread first way
#DEPTH_PRIORITY = 1
#SCHEDULER_DISK_QUEUE = 'scrapy.squeue.PickleFifoDiskQueue'
#SCHEDULER_MEMORY_QUEUE = 'scrapy.squeue.FifoMemoryQueue'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mp3tikkabot (+http://www.mp3tikka.com)'
