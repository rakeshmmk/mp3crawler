#!/usr/bin/env python
# encoding=utf-8

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.http import FormRequest
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from scrapy import log
from crawlers.items import FileItem
import sys
import re
import os

### Kludge to set default encoding to utf-8
reload(sys)
sys.setdefaultencoding('utf-8')
#sys.path.append(os.path.abspath("../../../"))

from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError

#from sqlalchemy.orm import relationship, backref
#Base = declarative_base()

from sqlalchemy.orm import sessionmaker
from model import File,Base,FileTag,TAG_MOVIE,TAG_SONG,SITE_WAPMALAYALAM

# Create an engine that stores data in the local directory's
# sqlalchemy_example.db file.
#engine = create_engine('sqlite:///database.db')
#engine = create_engine('sqlite:///database.db')
engine = create_engine('mysql://root:greatuser@localhost/crawler')
# Create all tables in the engine. This is equivalent to "Create Table"
# statements in raw SQL.
Base.metadata.create_all(engine)

# Bind the engine to the metadata of the Base class so that the
# declaratives can be accessed through a DBSession instance
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
# A DBSession() instance establishes all conversations with the database
# and represents a "staging zone" for all the objects loaded into the
# database session object. Any change made against the objects in the
# session won't be persisted into the database until you call
# session.commit(). If you're not happy about the changes, you can
# revert all of them back to the last commit by calling
# session.rollback()
session = DBSession()


class WapGsmSpider(CrawlSpider):
	name = "wapgsm"
	allowed_domains = ["wapgsm.in"]
	start_urls = [
		"http://wapgsm.in/Songs/"
	]
	rules = (
		Rule(SgmlLinkExtractor(allow=('index\.php', ),unique=True)),

		#When we get to specific doc do this
		Rule(SgmlLinkExtractor(allow=('file.\php', )), callback='parse_hansard_item'),

		)

	def parse_hansard_item(self, response):
		self.log('parse_hansard_item called for: %s' % response.url, level=log.INFO)
		hxs 		= HtmlXPathSelector(response)
		base_url	= "http://wapgsm.in/Songs/"
		item 		= FileItem()
		tags		= []
		#item['link'] 	= base_url+hxs.select("//html/body/div[8]/div/div/a/@href").extract()[0]
		#Direct links won't work in this site, hence saving page location instead of mp3 loc
		link		= response.url
		#print item['link']
		
		string 		= base_url+hxs.select("//html/body/div[8]/div/div").extract()[0]
		name		= re.search(re.compile('<b>File Name:<\/b>(.+)<br><b>Size'),string).group(1)
		tags.append(re.search(re.compile('<br>Album/Movie: (.+)<br>Ar'),string).group(1))
		#tags.append(re.search(re.compile('<br><b>Size:<\/b>(.+)<br><b>Ad'),string).group(1))
		#item['tags']	= tags
		#print item['name']
		#print item['tags']
		try:
			new_file = File(name=name,url=link,site="wapgsm.in")
			session.add(new_file)
			session.commit()
			for tag in tags:
				mov_tag  = FileTag(tag=tag,tag_type=TAG_MOVIE,fle=new_file)
				session.add(mov_tag)
				session.commit()
		except IntegrityError:
			self.log("Duplicate entry for url%s"%song_url)
			self.log("Rolling back session to recover from integrity Exception")
			session.rollback()
		

		return item
