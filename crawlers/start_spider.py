from twisted.internet import reactor
from scrapy.crawler import Crawler
from scrapy import log,signals
from crawlers.spiders.mp3_crawler import FollowAllSpider
from scrapy.utils.project import get_project_settings
from scrapy.xlib.pydispatch import dispatcher
from scrapy.resolver import CachingThreadedResolver
#first_crawler	= 1

urls = ['http://www.mymp3song.com/','http://www.musicplanet.in/','http://www.djmaza.info/','http://www.freshmaza.net.in/','http://www.tamiltunes.com/','http://www.f24.in/','http://www.wap.ind.in/','http://cinemusicwood.com/','http://newtamilmp3page.blogspot.in/']
url_index	= 0
crawlers_open	= 0
def getNextURL():
	global url_index
        ##-------------BEGIN Code to Fetch Start URLs from DB---------------------##

        #engine = create_engine(settings['SQLALCHEMY_ENGINE_URL'])
        #Base.metadata.bind = engine
        #DBSession = sessionmaker(bind=engine)
        #session = DBSession()
        #results = session.query(StartUrl)
        #for result in results:
        #       if result.active:
        #               self.urls.append(result.url)
                        #self.allowed_domains.append(urlparse(result.url).hostname.lstrip('www.'))
        ##-------------END Code to Fetch Start URLs from DB-----------------------##



	url_index = url_index+1
	return urls[url_index-1]


def setup_crawler(url,first_crawler):
	global crawlers_open
	spider = FollowAllSpider(url=url)
	settings = get_project_settings()
    	settings.overrides['TELNETCONSOLE_ENABLED'] = 0
    	settings.overrides['WEBSERVICE_ENABLED'] = False
	crawler = Crawler(settings)
	crawler.configure()
	if first_crawler:
		log.start(logfile=settings['LOG_FILE'],loglevel=settings['LOG_LEVEL'])
		#settings.overrides['TELNETCONSOLE_ENABLED'] = 1
	crawlers_open = crawlers_open + 1
	print "Opening crawler number: "+str(crawlers_open)
	crawler.crawl(spider)
	crawler.start()

# Get a signal that a crawler has finished its job, start another crawler
def spider_closed(spider, reason): 
	reactor.callLater(0, setup_crawler,url=getNextURL(),first_crawler=0)

def main():

	first_crawler   = 1
	number_of_concurrent_crawlers = 5

	# Subscribe to the "spider_closed" signals
	dispatcher.connect(spider_closed, signals.spider_closed)

	# Start a required number of concurrent crawlers
	for i in range(number_of_concurrent_crawlers):
	#	print "Opening Spider:"+str(i)
		reactor.callLater(0, setup_crawler,url=getNextURL(),first_crawler=first_crawler)
		first_crawler = 0
		
	reactor.installResolver(CachingThreadedResolver(reactor))

	# Run twisted reactor. This call is blocking.
	reactor.run(installSignalHandlers=False)
 
if __name__ == "__main__":
	main()


